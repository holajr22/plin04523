export interface Message {
    author: string;
    title: string;
    text: string;
}