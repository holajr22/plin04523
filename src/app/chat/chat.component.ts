import { Component } from '@angular/core';
import { Message } from '../models/message.model';
import { MessageManagerService } from '../services/message-manager.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent {
  public title: string = "";
  public author: string = "";
  public text: string = "";

  constructor(public manager: MessageManagerService) {}

  public submitMessage(title: string, author: string, text: string): void {
    let newMessage: Message = {
      title: title,
      author: author,
      text: text
    };
    this.manager.addMessage(newMessage);
    this.title = "";
    this.author = "";
    this.text = "";
  }
}
