import { Component } from '@angular/core';
import { Message } from './models/message.model';
import { MessageManagerService } from './services/message-manager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
}
