import { Injectable } from '@angular/core';
import { Message } from '../models/message.model';

@Injectable({
  providedIn: 'root'
})
export class MessageManagerService {
  private messages: Message[] = [];

  constructor() { }

  public getMessages(): Message[] {
    return this.messages;
  }

  public addMessage(message: Message): void {
    this.messages.push(message);
  }

  public deleteMessage(index: number) {
    this.messages.splice(index, 1);
  }
}
