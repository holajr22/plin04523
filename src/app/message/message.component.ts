import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent {
  @Input() public author: string = "ANONYM";
  @Input() public title: string = "";
  @Input() public text: string = "";
  @Output() public deleted: EventEmitter<void> = new EventEmitter<void>();

  public deleteMessage() {
    this.deleted.next();
  }
}
